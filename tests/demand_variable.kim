worldview im;

@test(
    name = "landcover.tests.demand.variable",
    observations = ("change in landcover:LandCoverType")
)
observe earth:Region named demand_variable
	over space(shape = "EPSG:4326 POLYGON ((-7.430546653452617 41.37041116258419, -2.2506923409638175 41.37041116258419, -2.2506923409638175 43.86425862069035, -7.430546653452617 43.86425862069035, -7.430546653452617 41.37041116258419))",
			   grid = 1.km), 
		 time(start=2010-01-01, end=2020-01-01);

/** 
 * Demand for agriculture falls in the second year and picks up again in the last two. Here using
 * explicit dates; could also use durations for each phase instead, i.e. 1.year, 7.years, 2.years,
 * with the same results. Uses a piecewise linear interpolation to assess demand at each timestep.
 */
@time(step=1.year)
model change in landcover:LandCoverType
	observing
	
		// predictors for the suitability model, linked by name to the resource
		geography:Slope,
		geography:Elevation,
		count of demography:HumanIndividual,
		earth:AtmosphericTemperature in Celsius
		
	using klab.landcover.allocate(
		suitability = luc.suitabilitymodel.simple,
		resistances = {
			landcover:Forest: 0.9,
			landcover:ScrubHerbaceousVegetation: 0.8,
			landcover:AgriculturalVegetation: 0.85
		},
		demand = {{
			landcover:AgriculturalVegetation | 2010-01-01 | 0.05, 
			landcover:AgriculturalVegetation | 2011-01-01 | 0, 
			landcover:AgriculturalVegetation | 2018-01-01 | 0, 
			landcover:AgriculturalVegetation | 2018-01-01 | 0.05 
		}},
		deviations = {
			landcover:AgriculturalVegetation: 0.03
		},
		transitions = {{
			       *                             | landcover:AgriculturalVegetation | landcover:Shrubland,
			landcover:ScrubHerbaceousVegetation  | true                             | true,
			landcover:Forest                     | true                             | true,
			landcover:AgriculturalVegetation     | true                             | true
		}}
	);
	
	
	